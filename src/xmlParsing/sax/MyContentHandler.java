package xmlParsing.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import xmlParsing.module.SeedSeller;

import java.util.ArrayList;
import java.util.List;

public class MyContentHandler extends DefaultHandler {
    private List<SeedSeller> seedSellerList = new ArrayList<>();
    SeedSeller tempSeedSeller = new SeedSeller();
    private boolean isHeader;
    private boolean isSeedSeller;
    private boolean isId;
    private boolean isRayon;
    private boolean isName;
    private boolean isCity;
    private boolean isDate;
    private boolean isBitki;
    private boolean isSort;
    private boolean isR;
    private boolean isNumber;

    @Override
    public void startDocument() throws SAXException {
        System.out.println("Start document");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        System.out.println("Start element " + qName);
        if(qName.equals("header")){
            System.out.println("Header reading...");
            isHeader = true;
        } else if (qName.equals("body")) {
            isHeader = false;
        }else if(qName.equals("SEED_SELLER")){
            tempSeedSeller = new SeedSeller();
            isSeedSeller = true;
        }else if(qName.equals("id") && !isHeader){
            isId = true;
        } else if(qName.equals("rayon") && !isHeader){
            isRayon = true;
        }else if(qName.equals("name") && !isHeader){
            isName = true;
        }else if(qName.equals("city") && !isHeader){
            isCity = true;
        }else if(qName.equals("date") && !isHeader){
            isDate = true;
        }else if(qName.equals("bitki") && !isHeader){
            isBitki = true;
        }if(qName.equals("sort") && !isHeader){
            isSort = true;
        } else if(qName.equals("r") && !isHeader){
            isR = true;
        }else if(qName.equals("number") && !isHeader){
            isNumber = true;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
       String data = new String(ch,start,length);
        System.out.println("data = " + data);
        if(isId){
            tempSeedSeller.setId(Long.parseLong(data));
        }else if(isRayon){
            tempSeedSeller.setRayon(data);
        }else if(isName){
            tempSeedSeller.setName(data);
        }else if(isCity){
            tempSeedSeller.setCity(data);
        }else if(isDate){
            tempSeedSeller.setDate(data);
        }else if(isBitki){
            tempSeedSeller.setBitki(data);
        }else if(isSort){
            tempSeedSeller.setSort(data);
        }else if(isR){
            tempSeedSeller.setR(data);
        }else if(isNumber){
            tempSeedSeller.setNumber(data);
        }
        System.out.println("TempSeedSeller " + tempSeedSeller);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        System.out.println("End element " + qName);
        if(qName.equals("header")){
            System.out.println("Header readed");
            isHeader = true;
        } else if (qName.equals("body")) {
            isHeader = false;
        }else if(qName.equals("SEED_SELLER")){
            isSeedSeller = false;
            seedSellerList.add(tempSeedSeller);
            tempSeedSeller = null;
            System.out.println(seedSellerList);
        }else if(qName.equals("id") && !isHeader){
            isId = false;
        } else if(qName.equals("rayon") && !isHeader){
            isRayon = false;
        }else if(qName.equals("name") && !isHeader){
            isName = false;
        }else if(qName.equals("city") && !isHeader){
            isCity = false;
        }else if(qName.equals("date") && !isHeader){
            isDate = false;
        }else if(qName.equals("bitki") && !isHeader){
            isBitki = false;
        }if(qName.equals("sort") && !isHeader){
            isSort = false;
        } else if(qName.equals("r") && !isHeader){
            isR = false;
        }else if(qName.equals("number") && !isHeader){
            isNumber = false;
        }
    }
    @Override
    public void endDocument() throws SAXException {
        System.out.println("End document");
    }

    public List<SeedSeller> getSeedSellerList() {
        return seedSellerList;
    }
}
