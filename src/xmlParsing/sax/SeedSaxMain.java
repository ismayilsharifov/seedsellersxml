package xmlParsing.sax;

import org.xml.sax.SAXException;
import xmlParsing.module.JDBCUtility;
import xmlParsing.module.SeedSeller;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.sql.*;
import java.util.List;
import java.util.ListIterator;

public class SeedSaxMain {
    public static void main(String[] args) throws ClassNotFoundException {
        String jdbcurl = "jdbc:oracle:thin:@localhost:1521/orcl";
        String useroracle = "User";
        String passwordoracle = "****";
        String postgreurl = "jdbc:postgresql://127.0.0.1:5432/mydatabase";
        String postgreuser = "user";
        String postgrepassword = "****";
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int counter = 0;

        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = factory.newSAXParser();
            MyContentHandler handler = new MyContentHandler();
            String uri = "https://sc.opendata.az/file/485";

            parser.parse(uri,handler);

            List<SeedSeller> seedSellerList = handler.getSeedSellerList();
            seedSellerList.forEach(System.out::println);

            connection = DriverManager.getConnection(jdbcurl,useroracle,passwordoracle);
            connection.setAutoCommit(false);
            System.out.println("Bazaya qoshuldu");

            String sql = " insert into seed_seller(seller_id,rayon,name,city,day,bitki,sort,r,phone_number)\n" +
                    "                    values(?,?,?,?,?,?,?,?,?)";
            ps = connection.prepareStatement(sql);
            ListIterator<SeedSeller> seedIter = seedSellerList.listIterator();
            while(seedIter.hasNext()){
                SeedSeller sd = seedIter.next();
                ps.setLong(1,sd.getId());
                ps.setString(2,sd.getRayon());
                ps.setString(3,sd.getName());
                ps.setString(4,sd.getCity());
                ps.setString(5,sd.getDate());
                ps.setString(6,sd.getBitki());
                ps.setString(7,sd.getSort());
                ps.setString(8,sd.getR());
                ps.setString(9,sd.getNumber());
                ps.addBatch();
                int count = ps.executeUpdate();
                counter += count;
            }
            System.out.println("cemi " + counter + " obyekt bazaya elave edildi.");
        } catch (ParserConfigurationException | SAXException | IOException | SQLException e) {
            e.printStackTrace();
        }finally {
            JDBCUtility.close(rs,ps,connection);
        }
    }

    /*  create table seed_seller(
        seller_id number,
        rayon varchar2(255),
        name varchar2(255),
    city varchar2(20),
    day varchar2(20),
    bitki varchar2(40),
    sort varchar2(40),
    r varchar2(20),
    phone_number varchar2(20),
    constraint seller_pk primary key (seller_id)
);
*/
}
