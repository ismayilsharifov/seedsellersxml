package xmlParsing.module;

public class SeedSeller {
    private long id;
    private String rayon;
    private String name;
    private String city;
    private String date;
    private String bitki;
    private String sort;
    private String r;
    private String number;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRayon() {
        return rayon;
    }

    public void setRayon(String rayon) {
        this.rayon = rayon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBitki() {
        return bitki;
    }

    public void setBitki(String bitki) {
        this.bitki = bitki;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getR() {
        return r;
    }

    public void setR(String r) {
        this.r = r;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "SeedSeller{" +
                "id=" + id +
                ", rayon='" + rayon + '\'' +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", date='" + date + '\'' +
                ", bitki='" + bitki + '\'' +
                ", sort='" + sort + '\'' +
                ", r='" + r + '\'' +
                ", number='" + number + '\'' +
                '}';
    }
}
